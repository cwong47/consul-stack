# Description
This repository has the steps and configurations to setup Hashicorp Consul stack in docker containers.
The stack utilizes the following softwares.
1. [docker](https://www.docker.com/)
2. [consul](https://www.consul.io/)
3. [acaleph/consul-alerts](https://github.com/AcalephStorage/consul-alerts)
4. [gliderlabs/registrator](https://github.com/gliderlabs/registrator)
5. [jippi/hashi-ui](https://github.com/jippi/hashi-ui) (optional tool)

# Requirement(s)
The following will need to be installed on your OSX.
1. docker
2. consul

# Instructions
The following steps are done on OSX and in the VMs. I will associate the hosts with either `osx$`, `consul-server$`, or `consul-client$`.

## Virtual Environment Setup
This script will setup the stack without interaction.
Once you have this repo cloned, all the commands are done inside the repo.
We will create 1 consul-server (default) and 5 consul-client nodes.
```
osx$ ./setup-stack.sh 5
...
osx$ docker-machine ls
NAME               ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER    ERRORS
consul-client001   -        virtualbox   Running   tcp://192.168.99.101:2376           v1.13.0
...
consul-client005   -        virtualbox   Running   tcp://192.168.99.105:2376           v1.13.0
consul-server001   *        virtualbox   Running   tcp://192.168.99.100:2376           v1.13.0
```

## Consul Server (Manually)
Lets log into the `consul-server` and start consul server and consul-alerts. I will be running this inside a `screen` session on OSX.
```
osx$ docker-machine ssh consul-server001

consul-server$ ls -l
total 2
drwxr-xr-x    3 docker   staff          200 Feb  5 21:17 consul.d/
consul-server$ docker run -d \
    -v $HOME/consul.d:/etc/consul.d \
    --name=consul-server \
    -e 'CONSUL_LOCAL_CONFIG={"datacenter": "sfo", "leave_on_terminate": false}' \
    -e 'CONSUL_CLIENT_INTERFACE=docker0' \
    -e 'CONSUL_BIND_INTERFACE=eth1' \
    --net=host \
    -p 8300:8300 \
    -p 8301:8301 \
    -p 8301:8301/udp \
    -p 8302:8302 \
    -p 8302:8302/udp \
    -p 8400:8400 \
    -p 8500:8500 \
    consul agent -dev \
    -advertise=$(ifconfig eth1 | grep 'inet addr' | awk '{print substr($2,6)}') \
    -client=0.0.0.0 \
    -node=consul \
    -ui \
    -config-dir=/etc/consul.d
```

Because the monitoring alerts I have will be checking port 8080, I will run a simple python http server.
```
consul-server$ docker run -d -p 8080:8080 cwong47/shttpd
```

Before we start `consul-alerts`, lets import the configs into `Key/Value`. Notice the file location, its actually looking for that file inside the container!
```
consul-server$ docker exec -t consul-server consul kv import @/etc/consul.d/consul-alerts/consul-alerts.config.json
```

Now we can start consul-alerts. Remember we're inside a `screen` session, and we will be running this container in interactive mode so we can tail the logs ...
```
consul-server$ docker run -it \
    -p 9000:9000 \
    acaleph/consul-alerts start \
    --consul-addr=$(ifconfig docker0 | grep 'inet addr' | awk '{print substr($2,6)}'):8500 \
    --consul-dc=sfo \
    --log-level=info \
    --watch-events \
    --watch-checks
```

Optionally, we can install jippi/hashi-ui, which is an awesome UI for Consul and Nomad.
```
consul-server$ docker run -it \
    --name=hashi-ui \
    -p 8585:3000 \
    jippi/hashi-ui \
    --consul-enable=1 \
    --consul-address=$(ifconfig eth1 | grep 'inet addr' | awk '{print substr($2,6)}'):8500
```

## Consul Client(s) (Manually)
Log into each of the `consul-client` nodes and start consul client, registrator.
We will define the IP address of the consul server from CLI in `CONSUL_SERVER`.
```
consul-client$ CONSUL_SERVER=192.168.99.100
consul-client$ docker run -d \
    -v $HOME/consul.d:/etc/consul.d \
    --name=consul-agent \
    -e 'CONSUL_LOCAL_CONFIG={"datacenter": "sfo", "rejoin_after_leave": true, "leave_on_terminate": true, "skip_leave_on_interrupt": true}' \
    -e 'CONSUL_CLIENT_INTERFACE=docker0' \
    -e 'CONSUL_BIND_INTERFACE=eth1' \
    --net=host \
    consul agent \
    -node=$HOSTNAME \
    -retry-join=$CONSUL_SERVER \
    -config-dir=/etc/consul.d
consul-client$ docker run -d \
    --net=host \
    --name=registrator \
    --volume=/var/run/docker.sock:/tmp/docker.sock \
    gliderlabs/registrator \
    -cleanup \
    consul://$(ifconfig docker0 | grep 'inet addr' | awk '{print substr($2,6)}'):8500
```

At this point, consul-alerts should be alerting API calls on port 8080 because thats one of the checks in `/etc/consul.d/*-check.json`.
Simply run a python http server to resolve the alert(s).
```
consul-client$ docker run -d -p 8080:8080 cwong47/shttpd
```

## Verification
Now that everything is setup, lets verify everything is running smoothly.
We can list all the nodes using the consul CLI.
```
osx$ export CONSUL_RPC_ADDR=192.168.99.100:8400
osx$ consul members
Node              Address              Status  Type    Build  Protocol  DC
consul            192.168.99.100:8301  alive   server  0.7.3  2         sfo
consul-client001  192.168.99.101:8301  alive   client  0.7.3  2         sfo
...
consul-client005  192.168.99.105:8301  alive   client  0.7.3  2         sfo
```

API calls via http.
```
osx$ curl -s 192.168.99.100:8500/v1/catalog/nodes
[
    {
        "ID": "ae20fd8e-c2fc-1cc2-9b7a-99affda36e02",
        "Node": "consul",
        "Address": "192.168.99.100",
        "TaggedAddresses": {
            "lan": "192.168.99.100",
            "wan": "192.168.99.100"
        },
        "Meta": {},
        "CreateIndex": 4,
        "ModifyIndex": 26
    },
    {
        "ID": "c4286aa9-2458-a230-360f-e9a76da23d56",
        ...
        "ModifyIndex": 95
    }
]
```

Or just point your browser to http://192.168.99.100/

## Consul-Alerts
I have the configs currently setup to notify slack for `consul-client.*` hosts, and only logs `consul-server00[1-2]` to log file.

### Logs
To tail the consul-alerts logs from consul-server, simply attach to the container. To detach, simply press `^P+^Q`.
```
osx$ eval $(docker-machine env consul-server001)
osx$ docker attach consul-alerts
...
^P+^Q
osx$ docker attach hashi-ui
...
^P+^Q
```

### K/V Backup
```
consul-server$ docker exec -t consul-server consul kv export consul-alerts/config/ > consul-alerts.config.json

osx$ docker-machine scp consul-server001:consul-alerts.config.json /tmp/
```

## Virtual Environment Clean-up
```
osx$ docker-machine ls | awk '$1 ~ /consul-(client|server)/ {print $1}' | xargs docker-machine rm -y
```

# License and Authors
Authors: Calvin Wong
