#!/usr/bin/env bash

#  Function(s)
#
fn_get_ip() {
    local _node="$1"
    local _iface="$2"

    docker-machine ssh $_node ifconfig $_iface | grep 'inet addr' | awk '{print substr($2,6)}'
}

fn_is_running() {
    local _node="$1"

    docker-machine ls | awk "\$1 ~ /${_node}/ && \$4 ~ /Running|Stopped/ {print \$1, \"is\", \$4}" | egrep "Running|Stopped"
}

fn_setup_vm() {
    local _node="$1"

    docker-machine create -d virtualbox $_node
    docker-machine scp -r etc/consul.d/ ${_node}:
}

#  Global Variable(s)
#
VM_COUNTS=${1:-5}
CONSUL_SERVER=consul-server001

#  Setup Consul Server, Consul-Alerts, Web-App, Load consul-alerts k/v json.
#
if ! fn_is_running $CONSUL_SERVER
then
    fn_setup_vm $CONSUL_SERVER

    CONSUL_SERVER_IP="$(fn_get_ip $CONSUL_SERVER eth1)"
    eval $(docker-machine env $CONSUL_SERVER)

    docker run -d \
        -v /home/docker/consul.d:/etc/consul.d \
        --name=consul-server \
        -e 'CONSUL_LOCAL_CONFIG={"datacenter": "sfo", "leave_on_terminate": false}' \
        -e 'CONSUL_CLIENT_INTERFACE=docker0' \
        -e 'CONSUL_BIND_INTERFACE=eth1' \
        --net=host \
        -p 8300:8300 \
        -p 8301:8301 \
        -p 8301:8301/udp \
        -p 8302:8302 \
        -p 8302:8302/udp \
        -p 8400:8400 \
        -p 8500:8500 \
        consul agent -dev \
        -advertise=$CONSUL_SERVER_IP \
        -client=0.0.0.0 \
        -node=consul \
        -ui \
        -config-dir=/etc/consul.d

    sleep 10
    docker exec -t consul-server consul kv import @/etc/consul.d/consul-alerts/consul-alerts.config.json

    docker run -d -p 8080:8080 cwong47/shttpd

    docker run -d -it \
        --name=consul-alerts \
        -p 9000:9000 \
        acaleph/consul-alerts start \
        --consul-addr=$(fn_get_ip $CONSUL_SERVER docker0):8500 \
        --consul-dc=sfo \
        --log-level=info \
        --watch-events \
        --watch-checks

    docker run -d -it \
        --name=hashi-ui \
        -p 8585:3000 \
        jippi/hashi-ui \
        --consul-enable=1 \
        --consul-address=${CONSUL_SERVER_IP}:8500
fi

#  Setup Consul Client(s), Registrator, Web-App
#
[ -z "$CONSUL_SERVER_IP" ] && CONSUL_SERVER_IP="$(fn_get_ip $CONSUL_SERVER eth1)"
for i in $(seq -f %03g 1 $VM_COUNTS)
do
    node=consul-client${i}

    if ! fn_is_running $node
    then
        fn_setup_vm $node

        eval $(docker-machine env $node)

        docker run -d \
            -v /home/docker/consul.d:/etc/consul.d \
            --name=consul-agent \
            -e 'CONSUL_LOCAL_CONFIG={"datacenter": "sfo", "rejoin_after_leave": true, "leave_on_terminate": true, "skip_leave_on_interrupt": true}' \
            -e 'CONSUL_CLIENT_INTERFACE=docker0' \
            -e 'CONSUL_BIND_INTERFACE=eth1' \
            --net=host \
            consul agent \
            -node=$node \
            -retry-join=$CONSUL_SERVER_IP \
            -config-dir=/etc/consul.d

        docker run -d \
            --net=host \
            --name=registrator \
            --volume=/var/run/docker.sock:/tmp/docker.sock \
            gliderlabs/registrator \
            -cleanup \
            consul://$(fn_get_ip $node docker0):8500

        docker run -d -p 8080:8080 cwong47/shttpd
    fi
done
